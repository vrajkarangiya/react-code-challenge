import React from 'react';
// import './App.scss';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

// components files added
import Converter from './components/Converter';

function App() {
  return (
    <div className="App">
      <Converter/>
    </div>
  );
}

export default App;
