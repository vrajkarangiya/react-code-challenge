import React from 'react'

const Header = () => {
  return (
    <div className='header-part'>
        <h2>Currency Converter</h2>
    </div>
  )
}

export default Header