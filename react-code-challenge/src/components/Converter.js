import React, { useState, useEffect } from 'react';
import axios from "axios";


// import files
import Header from './Header';


const Converter = () => {

    // initial Object for info
    const [initialList, setInitailList] = useState({
        currencies: ["USD", "SGD", "EUR", "INR"],
        base: "EUR",
        amount: "",
        convertTo: "INR",
        date: "",
    });
    const[rate,setRate] = useState("");

    // Destructuring state
    const { currencies, base, amount, convertTo, date } = initialList;
    useEffect(() => {
        if (amount === isNaN) {
            return;
        } else {
            const getCurrency = async () => {
                    const response = await axios.get(`http://localhost:4000/name/${base}`);
                    setRate ((response.data.rates[convertTo] * amount).toFixed(4));
                    const date = response.data.date;
                    setInitailList({ ...initialList, rate, date });
                };
               getCurrency();
        }
        console.count('first')
    },[base,amount,convertTo]);

    // chnage amount 
    const chnageInput = (e) => {
        e.preventDefault();
        setInitailList({ ...initialList, amount: e.target.value ,  date: null, });
    };

    // chnage dropdown name functionality
    const handleDropdown = (e) => {
        e.preventDefault();
        setInitailList({ ...initialList, [e.target.name]: e.target.value});
    };

    return (
        <>
            <div className='container'>
                <Header />
                <div className='row'>
                    <div className='card-wrapper'>
                        <div className='col-12 col-md-6'>
                            <div className='card-body'>
                                {/* Heading Information part.........*/}
                                <div className='card-heading'>
                                    <h5>
                                        {amount} {base} equals
                                    </h5>
                                    <h3>
                                        {amount === "" ? "0" : rate === null ? "waiting for output ..." : rate}
                                        {convertTo}
                                    </h3>
                                    <p>As of {amount === "" ? "" : date === null ? "" : date}</p>
                                </div>
                                <div className='card-input'>
                                    <form>
                                        <input type='text' value={amount} onChange={chnageInput} />
                                        <select name='base' value={base} onChange={handleDropdown}>
                                            {currencies.map((data) => {
                                                return (
                                                    <option key={data} value={data}>{data}</option>
                                                )
                                            })}
                                        </select>
                                        <br></br>
                                        <input type='text' value={amount === "" ? "0" : rate === null ? "waiting for output ..." : rate}
                                            onChange={chnageInput} />
                                        <select name='convertTo' value={convertTo} onChange={handleDropdown}>
                                            {currencies.map((data) => {
                                                return (
                                                    <option key={data} value={data}>{data}</option>
                                                )
                                            })}
                                        </select>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Converter